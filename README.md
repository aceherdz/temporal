**COPY EJEMPLO**


## descripción de la tarea:

- en la case pasada ya habíamos hecho las 3 clases que procesan una línea de copy, teniendo en cuenta eso:
	- deben tomar el copy que esta en este repositorio.
	- escribir una clase que se llame CopyFactory, no hereda de ninguna.
	- esa clase en el constructor (el __init__) debe definir un variable que se llama raizCopy[]
	- definir un método, que recibe una ruta de un archivo (esa ruta es la ruta del copy)
		- validar que exista el archivo
		- leer línea por línea el archivo  y con cada línea crear un campoAbstracto (como lo hicimos la clase pasada)
		- agregar ese campo abstracto a la variable raizCopy.
		
**Obviamente debe tener unas pruebas unitarias**

**incluir la lógica que consideren necesaria para que puedan lograrlo**

**no hace falta meter la logica que procese la linea, solo crear los campos de acuerdo al tipo, como la clase pasada**


